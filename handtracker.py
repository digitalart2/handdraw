
# Инициализация видеопотока
# cap = cv2.VideoCapture(2)
# cap.set(3, 1920)  # Установка ширины кадра в 1920 пикселей
# cap.set(4, 1080)

    # cap = cv2.VideoCapture(0)  # Use 0 for the default camera, or the camera index for a specific camera
    # cap.set(3, 640)
    # cap.set(4, 480)

import os
import time
import cv2
import mediapipe as mp

import asyncio
import websockets
import json
# from mediapipe.tasks import python
# from mediapipe.tasks.python import vision


        
async def handle_client(websocket, path):
     
    # Получите абсолютный путь к текущей директории, где находится ваш Python-скрипт
    current_directory = os.path.dirname(os.path.abspath(__file__))

    # Полный путь к файлу модели
    model_path = os.path.join(current_directory, 'gesture_recognizer.task')
    
    model_path = 'E:/work/NewDraw/hand_draw-2.0/gesture_recognizer.task'
    if os.path.exists(model_path):
        print("Файл модели существует!")
    else:
        print("Файл модели не найден. Убедитесь, что указали правильный путь.")

    BaseOptions = mp.tasks.BaseOptions
    GestureRecognizer = mp.tasks.vision.GestureRecognizer
    GestureRecognizerOptions = mp.tasks.vision.GestureRecognizerOptions
    VisionRunningMode = mp.tasks.vision.RunningMode
    
    options = GestureRecognizerOptions(
    base_options=BaseOptions(model_asset_path=model_path),
    running_mode=VisionRunningMode.VIDEO)
    recognizer = GestureRecognizer.create_from_options(options)
    
        
    # Initialize video capture
    cap = cv2.VideoCapture(0)
    cap.set(3, 1920)  # Установка ширины кадра в 1920 пикселей
    cap.set(4, 1080)
    prev_timestamp_ms = 0
    while True:
        ret, frame = cap.read()
        #await websocket.send("hi")
        # Flip the frame horizontally for natural hand movement
        frame = cv2.flip(frame, 1)

        # Convert the frame to RGB
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = mp.Image(image_format=mp.ImageFormat.SRGB, data=image)
      
        current_timestamp_ms = int(time.time() * 1000)

    # Calculate the time elapsed since the last frame
        elapsed_time_ms = current_timestamp_ms - prev_timestamp_ms

  # Recognize gestures
        gesture_results = recognizer.recognize_for_video(image,elapsed_time_ms)
        
        if gesture_results.gestures:
            # Get top gesture for each hand
            top_gestures = [gesture[0].category_name for gesture in gesture_results.gestures]
          #  print("Top Gestures:", top_gestures)
            if "Open_Palm" in top_gestures:
                print("Open hand detected!")                       
                if gesture_results.hand_landmarks:
                    for landmarks in gesture_results.hand_landmarks:
                        landmark=landmarks[8]
                        x, y = int(landmark.x * frame.shape[1]), int(landmark.y * frame.shape[0])
                        cv2.circle(frame, (x, y), 5, (255, 0, 0), cv2.FILLED)
                        #Создайте словарь с координатами landmark
                        landmark_data = {'x': x, 'y': y}
                        # Преобразуйте данные в формат JSON
                        json_data = json.dumps(landmark_data)
                        # Отправьте данные клиенту по WebSocket
                        await websocket.send(json_data)
                        await asyncio.sleep(0.00001)



        # Show the frame with annotations
        cv2.imshow('Hand Tracking', frame)

        if cv2.waitKey(1) == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
    
    

if __name__ == '__main__':
   # main()
       # Запустите WebSocket сервер на localhost и порту 8025
    server = websockets.serve(handle_client, 'localhost', 8025)
    print("WebSocket сервер запущен. Слушает ws://localhost:8025")

    # Запустите asyncio loop и обработку клиентов
    asyncio.get_event_loop().run_until_complete(server)
    asyncio.get_event_loop().run_forever()

