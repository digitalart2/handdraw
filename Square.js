class Square {
    constructor(x, y) {
      this.x = x;
      this.y = y;
      this.speedX = random() * 4 - 2;
      this.speedY = random() * 4 - 2;
      this.maxSize = random() * 10 + 10;
      this.size = random() * 3 + 2;
      this.vs = random() * 0.2 + 0.1;
      this.angleX = random() * 6.2;
      this.vax = random() * 0.6 - 0.3;
      this.angleY = random() * 6.2;
      this.vay = random() * 0.6 - 0.3;
      this.angle = 0;
      this.va = random() * 0.02 + 0.05;
      this.lightness = 10;
    }
  
    update() {
      this.x += this.speedX + sin(this.angleX);
      this.y += this.speedY + sin(this.angleY);
      this.size += this.vs;
      this.angleX += this.vax;
      this.angleY += this.vay;
      this.angle += this.va;
      if (this.lightness < 70) this.lightness += 0.25;
      if (this.size < this.maxSize) {
        push();
        translate(this.x, this.y);
        rotate(this.angle);
        fill(255, 245, 222);
        stroke(60, 81, 134);
        strokeWeight(2);
        rect(0, 0, this.size, this.size);
        pop();
  
      requestAnimationFrame(this.update.bind(this));
      }
    }
  }