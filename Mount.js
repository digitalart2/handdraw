

class Mount {
    constructor(x, y) {
      this.x = x;
      this.y = y;
      this.speedX = random() * 4 - 2;
      this.speedY = random() * 4 - 2;
      this.maxSize = random() * 17 + 10;
      this.size = 0;
      this.vs = random() * 0.2 + 0.25;
      this.angleX = random() * 6.2;
      this.vax = random() * 0.6 - 0.3;
      this.angleY = random() * 6.2;
      this.vay = random() * 0.6 - 0.3;
      this.angle = 0;
      this.va = random() * 0.005 + 0.01;
      this.lightness = 10;
  
      drawingContext.shadowOffsetX = 0;
      drawingContext.shadowOffsetY = 0; 
      drawingContext.shadowBlur = 10;
      drawingContext.shadowColor = 'rgba(229, 81, 55, 0.5)';
      //drawingContext.globalCompositeOperation = 'destination-over';
      drawingContext.globalCompositeOperation = 'source-over';
    }
  
    update() {
      this.x += sin(this.angleX);
      this.y += sin(this.angleY);
      this.size += this.vs;
      this.angleX += this.vax;
      this.angleY += this.vay;
      this.angle += this.va;
      if (this.lightness < 70)
       this.lightness += 0.25;
      if (this.size < this.maxSize) {
        push();
        fill(200, 200, 180);
        rectMode(CENTER);
        translate(this.x, this.y);
        rotate(this.angle);
        noStroke()
   //     strokeWeight(0.1);
     //   rect(0 -this.size/2, 0- this.size/2,this.size/4, this.size/4);
     star(0, 0, this.size, this.size*3, 12);
        pop();
        setTimeout(() => {
          requestAnimationFrame(this.update.bind(this));
        }, 200); 
      }
    }
  }


  function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
  
  
  