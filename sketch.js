let socket;
var data;
let flag = false;

const Modes = {
  Weed: 1,
  Hexagon: 2,
  Pyramid: 3,
  Square: 4,
  //Mount: 5
  //Hedgenhog: 6
};
let maxObjs = 100;//максимально количество объектов
let randomValue = 3;
let objCount = 0;
let isDrawingAllowed = true;
let timer = 4

function setup() {
  createCanvas(windowWidth, windowHeight);
  socket = new WebSocket('ws://localhost:8025'); // Используйте точно такой же URL
  // frameRate(60);
  // Обработка события открытия соединения
  socket.onopen = function () {
    console.log('Connected to WebSocket server');
  };

  // Обработка входящих сообщений от WebSocket сервера
  socket.onmessage = function (event) {
    data = JSON.parse(event.data);
    flag = true;
    // x = data.x;
    // y = data.y;
    // Обработайте полученные координаты x и y здесь
  //  console.log('Получены координаты - x:', data.x, 'y:', data.y);
  };

  // Обработка события закрытия соединения
  socket.onclose = function () {
    console.log('Disconnected from WebSocket server');
  };
}

function draw() {
  // background(0);
  if(objCount>maxObjs)
  {
    isDrawingAllowed = false;
    if (frameCount % 60 == 0 && timer > 0) { 
      timer --;
    }
  
    if (timer == 0){
      background(255);
      isDrawingAllowed = true; 
      randomValue = Math.floor(Math.random() * Object.keys(Modes).length) + 1;
      timer = 4;
      objCount = 0
    }
  }

  if (flag && isDrawingAllowed) 
  { // Проверяем, есть ли данные (флаг установлен в true)
    switch (randomValue) {
      case Modes.Hedgenhog:
        drawHedgehog(data.x,data.y)
        break;
     case Modes.Weed:
       drawWeed(data.x,data.y)
       break;
     case Modes.Hexagon:
       drawHexagon(data.x,data.y);
       break;
     case Modes.Pyramid:
       drawPyramid(data.x,data.y);
     break;
     case Modes.Square:
       drawSquare(data.x,data.y);
     break;
     case Modes.Mount:
      drawMount(data.x,data.y);
     break;
     default:
       console.log("Неизвестное значение");
       break;
   }
    objCount++;
    flag = false; // Сбрасываем флаг обратно в false после отрисовки круга
  }

}

// function mousePressed() {
//   for (let i = 0; i < 3; i++) {
//     const root = new Pyramid(mouseX, mouseY);
//     root.update()
//   }
// }

function drawMount(x,y){
  for (let i = 0; i < 1; i++) {
    const root = new Mount(x,y);
    root.update();
  }
}


function drawHedgehog(x,y){
  for (let i = 0; i < 3; i++) {
    const root = new Hedgehog(x,y);
    root.update()
  }
}

function drawWeed(x,y){
  for (let i = 0; i < 3; i++) {
    const root = new Weed(x,y);
    root.update()
   
  }
}

function drawHexagon(x,y){
  for (let i = 0; i < 3; i++) {
    const root = new Hexagon(x,y);
    root.update()
  }
}

function drawPyramid(x,y){
  for (let i = 0; i < 3; i++) {
    const root = new Pyramid(x,y);
    root.update()
  }
}

function drawSquare(x,y){
  for (let i = 0; i < 3; i++) {
    const root = new Square(x,y);
    root.update()
  }
}
