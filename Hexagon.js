
class Hexagon {
    constructor(x, y) {
      this.x = x;
      this.y = y;
      this.speedX = random() * 4 - 2;
      this.speedY = random() * 4 - 2;
      this.maxSize = random() * 10 + 10;
      this.size = random() * 3 + 2;
      this.vs = random() * 0.2 + 0.1;
      this.angleX = random() * 6.2;
      this.vax = random() * 0.6 - 0.3;
      this.angleY = random() * 6.2;
      this.vay = random() * 0.6 - 0.3;
      this.angle = 0;
      this.va = random() * 0.2 + 0.5;
      this.lightness = 10;

      drawingContext.shadowOffsetX = 0;
      drawingContext.shadowOffsetY = 10; // Устанавливаем смещение тени по горизонтали
      drawingContext.shadowBlur = 10;   
      drawingContext.shadowColor = 'rgba(0, 0, 255, 0.5)';
    }
  
    update() {
      this.x += this.speedX + sin(this.angleX);
      this.y += this.speedY + sin(this.angleY);
      this.size += this.vs;
      this.angleX += this.vax;
      this.angleY += this.vay;
      this.angle += this.va;
      if (this.lightness < 70) this.lightness += 0.25;
      if (this.size < this.maxSize) {
       
        push();
        translate(this.x, this.y);
        rotate(this.angle);
        fill(0, 0, 0, 0);
        stroke(255, 0, 255);
        rectMode(CENTER);
        beginShape();
        vertex(-this.size/2, -this.size/2);
        vertex(-this.size/4, -this.size/2 - this.size/2);
        vertex(this.size/4, -this.size/2 - this.size/2);
        vertex(this.size/2, -this.size/2);
        vertex(this.size/4, -this.size/2 + this.size/2);
        vertex(-this.size/4, -this.size/2 + this.size/2);
        endShape(CLOSE);
        pop();

      requestAnimationFrame(this.update.bind(this));
   
      }
    }
  }
  
