
class Weed {
    constructor(x, y) {
      this.x = x;
      this.y = y;
      this.speedX = random() * 4 - 2;
      this.speedY = random() * 4 - 2;
      this.maxSize = random() * 10 + 4;
      this.size = random() * 7 + 2;
      this.vs = random() * 0.2 + 0.1;
      this.angleX = random() * 6.2;
      this.vax = random() * 0.6 - 0.3;
      this.angleY = random() * 6.2;
      this.vay = random() * 0.6 - 0.3;
      this.angle = 0;
      this.va = random() * 0.4 + 0.05;
      this.lightness = 10;

      drawingContext.shadowOffsetX = 0;
      drawingContext.shadowOffsetY = 0; // Устанавливаем смещение тени по горизонтали
      drawingContext.shadowBlur = 5; 
      drawingContext.shadowColor = 'rgba(0, 0, 0, 1)'; // Черный цвет с полной непрозрачностью
      //drawingContext.shadowColor = 'rgba(255, 255, 255, 1)';   
     // drawingContext.globalCompositeOperation = 'destination-over';
      drawingContext.globalCompositeOperation = 'source-over';
    }
  
  
  
    update() {
      this.x += this.speedX + sin(this.angleX);
      this.y += this.speedY + sin(this.angleY);
      this.size += this.vs;
      this.angleX += this.vax;
      this.angleY += this.vay;
      this.angle += this.va;
      if (this.lightness < 70) this.lightness += 0.25;
      if (this.size < this.maxSize) {
        push();
        strokeWeight(1); // Увеличение толщины обводки для эффекта неона
        translate(this.x, this.y);
        rotate(this.angle);
        fill(0, 0, 0, 0);
        var r = map(sin(frameCount /2),-1,1,100,200)
        var g = map(250,0,50,100,200)
        var b = map(cos(frameCount),-1,1,200,100)
        stroke(r,g,b)
        rectMode(CENTER);
        beginShape();
        vertex(-this.size/2 * 3, -this.size/2 * 3);
        vertex(-this.size/4 * 3, -this.size/2 * 3 - this.size/2 * 3);
        vertex(this.size/4 * 3, -this.size/2 * 3 - this.size/2 * 3);
        vertex(this.size/2 * 3, -this.size/2 * 3);
        vertex(this.size/4 * 3, -this.size/2 * 3 + this.size/2 * 3);
        vertex(-this.size/4 * 3, -this.size/2 * 3 + this.size/2 * 3);
        endShape(CLOSE);   
        pop();

          requestAnimationFrame(this.update.bind(this));
      
      }
    }
    
  }
  
