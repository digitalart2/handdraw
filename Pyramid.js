

class Pyramid {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.speedX = random() * 4 - 2;
    this.speedY = random() * 4 - 2;
    this.maxSize = random() * 17 + 10;
    this.size = 0;
    this.vs = random() * 0.2 + 0.25;
    this.angleX = random() * 6.2;
    this.vax = random() * 0.6 - 0.3;
    this.angleY = random() * 6.2;
    this.vay = random() * 0.6 - 0.3;
    this.angle = 0;
    this.va = random() * 0.02 + 0.05;
    this.lightness = 10;

    drawingContext.shadowOffsetX = 0;
    drawingContext.shadowOffsetY = 10; 
    drawingContext.shadowBlur = 10;
    drawingContext.shadowColor = 'rgba(0, 0, 255, 0.5)';
    //drawingContext.globalCompositeOperation = 'destination-over';
    drawingContext.globalCompositeOperation = 'source-over';
  }

  update() {
    this.x += this.speedX + sin(this.angleX);
    this.y += this.speedY + sin(this.angleY);
    this.size += this.vs;
    this.angleX += this.vax;
    this.angleY += this.vay;
    this.angle += this.va;
    if (this.lightness < 70) this.lightness += 0.25;
    if (this.size < this.maxSize) {
      push();
      fill(255, 245, 222);
      rectMode(CENTER);
      stroke(60, 81, 134);
      strokeWeight(0.5);
      translate(this.x, this.y);
      rotate(this.angle);
      strokeWeight(0.1);
      rect(0 -this.size/2, 0- this.size/2,this.size, this.size);
      strokeWeight(0.1);
      pop();
      
      requestAnimationFrame(this.update.bind(this));
      
  
    }
  }
}


