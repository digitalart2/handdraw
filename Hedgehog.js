


let hue = 0;

class Hedgehog {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.speedX = random() * 2 - 1;
    this.speedY = random() * 2 - 1;
    this.maxSize = random() * 17 + 10;
    this.size = random() * 15 + 8;
    this.vs = random() * 0.1 + 0.25;
    this.angleX = random() * 6.2;
    this.vax = random() * 5.6 - 0.3;
    this.angleY = random() * 6.2;
    this.vay = random() * 5.6 - 0.3;
    this.angle = 0;
    this.va = random() * 0.02 + 0.05;
    this.lightness = 10;

    drawingContext.shadowOffsetX = 0;
    drawingContext.shadowOffsetY = 10; // Устанавливаем смещение тени по горизонтали
    drawingContext.shadowBlur = 10;
    drawingContext.shadowColor = "black";
    stroke(`hsl(${hue}, 100%, 50%)`);
    hue = (hue + 3) % 360;
  }

  update() {
    this.x += this.speedX + sin(this.angleX);
    this.y += this.speedY + sin(this.angleY);
    this.size += this.vs;
    this.angleX += this.vax;
    this.angleY += this.vay;
    this.angle += this.va;
    if (this.lightness < 70) this.lightness += 0.25;
    if (this.size < this.maxSize) {
      push();
      rectMode(CENTER);

      strokeWeight(0.5);
      translate(this.x, this.y);
    for (let i = 0; i < 24; i++) {
        let angle = map(i, 0, 24, 0, TWO_PI); // Распределяем углы от 0 до 2*pi  
        let x =  cos(angle) * this.size * 5 ; // Вычисляем координаты конца луча по X
        let y =  sin(angle) * this.size * 5; // Вычисляем координаты конца луча по Y
      //  stroke(221, 160, 221);
        line(0, 0, x, y);
    }
      pop();
      requestAnimationFrame(this.update.bind(this));
      
    }
  }
}

